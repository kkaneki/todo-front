import { ADD_TASK, TOGGLE_TASK } from '../actions';

const initialState = {
    toDoList: []
};

// Root redux reducer
const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TASK:
            return {
                toDoList: [...state.toDoList, action.payload]
            };

        case TOGGLE_TASK:
            return {
                toDoList: action.payload
            };

        default:
            return state;
    }
};

export default rootReducer;

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';

import { addTask } from '../../actions';

import { api } from '../../services/todoApi';

class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            error: null
        }
    }

    // Handles change
    handleChange(task) {
        this.setState({
            name: task.target.value
        })
    }

    // Handles enter value of form
    handleKeyPress(e) {
        if (e.key === 'Enter') {
            api.createTodo(this.state.name)
                .then((response) => response.json())
                .then((data) => {


                    if (data.error) {
                        this.setState({
                            error: data.error.name[0]
                        })
                    } else {
                        const { id, name, completed } = data.data;
                        this.props.addTask({
                            id,
                            name,
                            completed
                        });

                        this.setState({
                            name: '',
                            error: null
                        })
                    }
                })
        }
    }

    render() {
        const { error } = this.state;
        return (
            <FormControl>
                <InputLabel htmlFor="task">Add awesome task:)</InputLabel>
                <Input id="task"
                       value={this.state.name}
                       onChange={(input) => this.handleChange(input)}
                       onKeyPress={(input) => this.handleKeyPress(input)}
                />
                <FormHelperText id="name-error-text">{error}</FormHelperText>
            </FormControl>
        );
    }
}

function mapStateToProps (state) {
    return {
        toDoList: state.toDoList
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addTask: bindActionCreators(addTask, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form);

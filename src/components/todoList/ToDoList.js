import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import _ from 'lodash';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox'

import { data } from '../../services/todoApi';
import {toggleTask} from '../../actions';

import './toDoList.css';

import { api } from '../../services/todoApi'

class ToDoList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            toDoList: []
        }
    }

    componentDidMount() {
        this.setState({
            toDoList: this.props.toDoList
        });
    }

    // Mark a ticket as completed
    handleToggle(id) {
        const toDo = _.clone(this.props.toDoList);
        const index = _.findIndex(toDo, ['id', id]);


        api.updateTodo(toDo[index].id)
            .then((response) => response.json())
            .then((data) => {
                toDo[index].completed = !toDo[index].completed;
                this.props.toggleTask(toDo);
            })
    }

    // Renders all tickets from redux state.
    renderToDo() {
        return this.props.toDoList.map((todo) => {
            const completedClass = todo.completed ? 'completed' : '';

            return (
            <div class="list-items">
                    <ListItem
                        key={todo.id}
                        dense
                        button
                        className={completedClass}
                    >
                        <ListItemText className="list-text" primary={todo.name} />
                        <ListItemSecondaryAction>
                            <Checkbox
                                onChange={() => this.handleToggle(todo.id)}
                                checked={todo.completed}
                            />
                        </ListItemSecondaryAction>
                    </ListItem>
            </div>
            )
        });
    }

    render() {
        return (
            <List>
                {this.renderToDo()}
            </List>
        )
    }
}

function mapStateToProps (state) {
    return {
        toDoList: state.toDoList
    }
}

function mapDispatchToProps(dispatch) {
    return {
        toggleTask: bindActionCreators(toggleTask, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);

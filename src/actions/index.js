export const ADD_TASK = 'ADD_TASK';
export const TOGGLE_TASK = 'TOGGLE_TASK';

// Redux action
// Add task
export const addTask = (task) => ({
    type: ADD_TASK,
    payload: task
});

// Redux action
// Mark as completed task
export const toggleTask = (tasks) => ({
        type: TOGGLE_TASK,
        payload: tasks
});

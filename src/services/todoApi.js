// Base api URL
const BASE_URL = 'http://localhost:6543/api/';

export const api = {
    // Base method for calls
    makeRequest(endpoint, method, body) {
        return fetch(
            BASE_URL + endpoint,
            {
                method: method,
                body: JSON.stringify(body)
            }
        )
    },

    // Create a ticket
    createTodo(name) {
        return this.makeRequest('todo/new', 'POST', {name: name})
    },

    // Mark ticket as completed
    updateTodo(id) {
        return this.makeRequest(`todo/${id}/toggle`, 'PATCH', {})
    }
};

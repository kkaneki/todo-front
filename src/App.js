// eslint-disable-next-line
import React, { Component } from 'react';

import './App.css';
import ToDoList from './components/todoList/ToDoList';
import Form from './components/form/Form';

const App = () => (
    <div className="main-form">
        <Form />
        <ToDoList />
    </div>
);

export default App;
